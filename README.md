# README #

REQUIREMENTS
------------

The minimum requirement by this application template that your Web server supports PHP 5.4.0.


#### COMPOSER

**on root web app**

```html
php composer.phar global require "fxp/composer-asset-plugin:1.0.0-beta4" or newer
run: php composer.phar install or php composer.phar update
```
[More information](https://getcomposer.org/doc/00-intro.md)

### Database

Edit the file 
`config/mode_prod.php` (for prodaction)
`config/mode_test.php` (for testing)
switch mode in `config/mode.php`
with real data, for example:

```php
'db' => [
 'dsn' => 'mysql:host=localhost;dbname=dbname',
 'username' => 'root',
 'password' => 'secret',
 'enableSchemaCache' => false,
 'schemaCacheDuration' => 86400
],
```

#### Run migration

```php
yii migrate
```

#### For testing rest api 
use `http://{SITE_URL}/rest/tools/console` tool