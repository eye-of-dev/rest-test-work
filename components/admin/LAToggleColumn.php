<?php

namespace app\components\admin;

use dosamigos\grid\ToggleColumn;

class LAToggleColumn extends ToggleColumn
{

    public $onValue = 1;
    public $onLabel = 'Дв';
    public $offLabel = 'Нет';
    public $contentOptions = ['class' => 'col-md-1 text-center'];
    public $headerOptions = ['class' => 'col-md-1'];
    public $onIcon = 'glyphicon glyphicon-ok-circle btn btn-success';
    public $offIcon = 'glyphicon glyphicon-remove-circle btn btn-default';
    public $filter = [
        1 => 'Активный',
        0 => 'Не активный'
    ];

}
