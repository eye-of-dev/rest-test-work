<?php

use yii\db\Migration;
use yii\db\Schema;

class m151120_162857_initMigration extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        //Users table
        $this->createTable('users', [
            'id' => 'pk',
            'username' => Schema::TYPE_STRING . ' NOT NULL',
            'password' => Schema::TYPE_STRING . ' NOT NULL',
            'first_name' => Schema::TYPE_STRING,
            'last_name' => Schema::TYPE_STRING,
            'birthday' => Schema::TYPE_DATE . ' DEFAULT "0000-00-00"',
            'avatar' => Schema::TYPE_STRING,
            'create_date' => Schema::TYPE_TIMESTAMP . ' DEFAULT "0000-00-00 00:00:00"',
            'update_date' => Schema::TYPE_TIMESTAMP . ' DEFAULT "0000-00-00 00:00:00"',
            'is_active' => Schema::TYPE_SMALLINT . ' DEFAULT 0',
            'role' => Schema::TYPE_STRING . ' DEFAULT "user"',
                ], $tableOptions);

        $this->createIndex('is_active_users', 'users', 'is_active');

        echo 'DEFAULT USERNAME: admin';
        echo 'DEFAULT PASSWORD: 123456';

        $this->insert('users', [
            'username' => 'admin',
            'password' => '$2y$13$.TW4UJ.e8cTdTusrMH4hd.HAtuTxAxfd04njOsemp5JY5ZG/IvhXm',
            'is_active' => 1,
            'role' => 'admin'
        ]);

        $this->createTable('users_auth_keys', [
            'id' => 'pk',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'auth_key' => Schema::TYPE_STRING . ' NOT NULL',
            'expires' => Schema::TYPE_STRING . ' NOT NULL',
            'create_date' => Schema::TYPE_TIMESTAMP . ' DEFAULT "0000-00-00 00:00:00"',
            'update_date' => Schema::TYPE_TIMESTAMP . ' DEFAULT "0000-00-00 00:00:00"',
                ], $tableOptions);

        $this->createIndex('user_id', 'users_auth_keys', 'user_id');
        $this->addForeignKey('user_id_users_auth_keys_fk', 'users_auth_keys', 'user_id', 'users', 'id');

        $this->createIndex('auth_key', 'users_auth_keys', 'auth_key');
        $this->createIndex('expires', 'users_auth_keys', 'expires');
        
        //Config table
        $this->createTable('config', [
            'id' => 'pk',
            'param' => Schema::TYPE_STRING . ' NOT NULL',
            'value' => Schema::TYPE_STRING . ' NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL'
                ], $tableOptions);

        //Projects table
        $this->createTable('projects', [
            'id' => 'pk',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'date_start' => Schema::TYPE_DATE . ' DEFAULT "0000-00-00"',
            'date_end' => Schema::TYPE_DATE . ' DEFAULT "0000-00-00"',
            'description' => Schema::TYPE_TEXT . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "исполнитель"',
            'project_owner' => Schema::TYPE_STRING . ' NOT NULL COMMENT "владелец проекта"',
            'status' => Schema::TYPE_INTEGER . ' NOT NULL',
            'is_delete' => Schema::TYPE_SMALLINT . ' DEFAULT 0',
            'create_date' => Schema::TYPE_TIMESTAMP . ' DEFAULT "0000-00-00 00:00:00"',
            'update_date' => Schema::TYPE_TIMESTAMP . ' DEFAULT "0000-00-00 00:00:00"',
                ], $tableOptions);

        $this->createIndex('status_projects', 'projects', 'status');
        $this->createIndex('is_delete_projects', 'projects', 'is_delete');

        $this->createIndex('user_id_issues', 'issues', 'user_id');
        $this->addForeignKey('FK_issues_users_user_id', 'issues', 'user_id', 'users', 'id');
        
        //Issues table
        $this->createTable('issues', [
            'id' => 'pk',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'description' => Schema::TYPE_TEXT . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "исполнитель"',
            'status' => Schema::TYPE_INTEGER . ' NOT NULL',
            'is_active' => Schema::TYPE_SMALLINT . ' DEFAULT 0',
            'is_delete' => Schema::TYPE_SMALLINT . ' DEFAULT 0',
            'date_end' => Schema::TYPE_DATE . ' DEFAULT "0000-00-00" COMMENT "директивные сроки"',
            'create_date' => Schema::TYPE_TIMESTAMP . ' DEFAULT "0000-00-00 00:00:00"',
            'update_date' => Schema::TYPE_TIMESTAMP . ' DEFAULT "0000-00-00 00:00:00"',
                ], $tableOptions);

        $this->createIndex('status_issues', 'issues', 'status');
        $this->createIndex('is_active_issues', 'issues', 'is_active');
        $this->createIndex('is_delete_issues', 'issues', 'is_delete');

        $this->createIndex('user_id_issues', 'issues', 'user_id');
        $this->addForeignKey('FK_issues_users_user_id', 'issues', 'user_id', 'users', 'id');
        
        $this->createTable('logs', [
            'id' => 'pk',
            'user_id' => Schema::TYPE_INTEGER,
            'module' => Schema::TYPE_STRING,
            'item_id' => Schema::TYPE_INTEGER,
            'message' => Schema::TYPE_STRING,
            'create_date' => Schema::TYPE_TIMESTAMP . ' DEFAULT "0000-00-00 00:00:00"',
        ], $tableOptions);

        $this->createIndex('user_id_index', 'logs', 'user_id');
        $this->addForeignKey('user_id_log_fk', 'logs', 'user_id', 'users', 'id');
    }

    public function down()
    {
        $this->dropTable('config');
        $this->dropTable('projects');
        $this->dropTable('issues');
        $this->dropTable('logs');
        $this->dropTable('users_auth_keys');
        $this->dropTable('users');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
