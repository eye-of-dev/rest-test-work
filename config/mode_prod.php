<?php
return [
    'yiiDebug' => true,
    'yiiEnv' => 'test',
    'web' => [
        'components' => [
            'db' => [
                'dsn' => 'mysql:host=localhost;dbname=sgmsoft',
                'username' => 'root',
                'password' => 'secret',
                'enableSchemaCache' => false,
                'schemaCacheDuration' => 86400
            ],
        ],
    ],
    'console' => [
        'components' => [
            'db' => [
                'class' => '\yii\db\Connection',
                'dsn' => 'mysql:host=localhost;dbname=sgmsoft',
                'username' => 'root',
                'password' => 'secret',
                'enableSchemaCache' => false,
                'schemaCacheDuration' => 86400
            ],
        ],
    ]
];
