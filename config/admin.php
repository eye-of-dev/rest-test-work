<?php

$result = [
    'web' => [
        'basePath' => dirname(__DIR__),
        'language' => 'ru',
        'defaultRoute' => 'mainpage/default/index',
        'components' => [
            'assetManager' => [
                'bundles' => [
                    'app\assets\AppAsset' => [
                        'packages' => ['admin-main', 'depends']
                    ],
                ],
            ],
            'session' => [
                'timeout' => 86400 * 30
            ],
            'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => true,
                'rules' => [
                    '<module>/<action>' => '<module>/default/<action>',
                    '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                ]
            ],
            'user' => [
                'identityClass' => 'app\modules\users\models\UserIdentity',
                'enableAutoLogin' => true,
                'loginUrl' => ['/users/default/login'],
                'authTimeout' => 86400 * 30,
                'absoluteAuthTimeout' => 86400 * 30
            ],
        ],
        'params' => [
            'yiiEnd' => 'admin'
        ],
    ],
];
return $result;
