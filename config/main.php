<?php

mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
setlocale(LC_ALL, 'ru_RU.UTF-8');
setlocale(LC_NUMERIC, 'ru_RU.UTF-8');
date_default_timezone_set('Europe/Moscow');

$app = dirname(__DIR__);
$vendorPath = $app . '/vendor';
$result = [
    'yiiPath' => $vendorPath . '/yiisoft/yii2/Yii.php',
    'yiiDebug' => true,
    'aliases' => array(
        'vendor' => $vendorPath,
        'root' => $app,
        'web' => $app . '/web',
        'media' => $app . '/web/media',
        'tests' => $app . '/tests',
        'console' => $app . '/console',
    ),
    'classMap' => [
        'yii\helpers\StringHelper' => $app . '/helpers/StringHelper.php',
        'yii\helpers\FileHelper' => $app . '/helpers/FileHelper.php',
        'yii\helpers\ArrayHelper' => $app . '/helpers/ArrayHelper.php',
        'yii\helpers\Url' => $app . '/helpers/Url.php'
    ],
    'web' => array(
        'basePath' => dirname(__DIR__),
        'name' => 'promo.tvshop',
        'id' => 'promo.tvshop',
        'language' => 'ru',
        'sourceLanguage' => 'ru',
        'bootstrap' => ['log'],
        'timeZone' => 'Europe/Moscow',
        'defaultRoute' => 'mainpage/default/index',
        'modules' => [
            'config' => [
                'class' => 'app\modules\config\Module'
            ],
            'issues' => [
                'class' => 'app\modules\issues\Module'
            ],
            'logs' => [
                'class' => 'app\modules\logs\Module',
            ],
            'mainpage' => [
                'class' => 'app\modules\mainpage\Module',
            ],
            'projects' => [
                'class' => 'app\modules\projects\Module',
            ],
            'rest' => [
                'class' => 'app\modules\rest\Module',
            ],
            'users' => [
                'class' => 'app\modules\users\Module'
            ],
            'debug' => [
                'class' => 'yii\debug\Module',
                'allowedIPs' => ['*']
            ]
        ],
        'components' => [
            'request' => [
                'enableCsrfValidation' => true,
                'cookieValidationKey' => 'sdfepioDzxqwf3246dfgkljdsa'
            ],
            'cache' => [
                'class' => 'yii\caching\FileCache'
            ],
            'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false,
            ],
            'user' => [
                'identityClass' => 'app\modules\users\models\UserIdentity',
                'enableAutoLogin' => true,
            ],
            'errorHandler' => [
                'errorAction' => 'mainpage/default/error',
            ],
            'mailer' => [
                'class' => 'yii\swiftmailer\Mailer',
            ],
            'log' => [
                'traceLevel' => 3,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'formatter' => [
                'defaultTimeZone' => 'Asia/Almaty',
                'timeZone' => 'Asia/Almaty',
            ],
            'db' => [
                'class' => 'yii\db\Connection',
                'charset' => 'utf8',
            ],
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'yii\i18n\DbMessageSource'
                    ],
                ],
            ],
        ],
        'params' => [
        ],
    ),
    'console' => [
        'id' => 'basic-console',
        'basePath' => dirname(__DIR__),
        'bootstrap' => ['log'],
        'controllerNamespace' => 'app\console\controllers',
        'components' => [
            'log' => [
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
        ],
    ],
];
return $result;
