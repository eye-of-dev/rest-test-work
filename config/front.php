<?php
$result = [
    'web' => array(
        'modules' => array(),
        'components' => [
            'assetManager' => [
                'appendTimestamp' => true,
                'bundles' => [
                    'app\assets\AppAsset' => [
                        'packages' => ['main', 'vendor', 'depends']
                    ],
                ],
            ],
            'user' => [
                'loginUrl' => ['/users/default/login'],
            ],
            'urlManager' => [
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                'suffix' => '',
                'rules' => [
                    '/' => 'mainpage/default/index',
                    '<module>/<action>' => '<module>/default/<action>',
                    '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                ]
            ],
            'errorHandler' => [
                'errorAction' => 'mainpage/default/error',
            ],
        ],
        'params' => [
            'yiiEnd' => 'front'
        ],
    ),
];
return $result;
