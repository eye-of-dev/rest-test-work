<?php
/* @var $this \yii\web\View */
/* @var $content string */
?>
<?php $this->beginPage() ?>
<!doctype html>
<html class="no-js">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= $this->title ?></title>
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <?php $this->head(); ?>
    </head>
    <body>
    <!-- End Google Tag Manager -->
    <?php $this->beginBody() ?>
        <div class="container">
            <?= $content ?>
        </div>
    <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage() ?>
