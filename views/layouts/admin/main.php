<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'test.work',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if (!Yii::$app->getUser()->getIsGuest() && Yii::$app->getUser()->getIdentity()->isAdmin()) {
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                [
                    'label' => 'Проекты', 
                    'visible' => Yii::$app->getUser()->getIdentity()->checkRole(['admin']),
                    'url' => ['/projects/default/index'],
                ],
                [
                    'label' => 'Задания', 
                    'visible' => Yii::$app->getUser()->getIdentity()->checkRole(['admin']),
                    'url' => ['/issues/default/index'],
                ],
                [
                    'label' => 'Пользователи', 
                    'visible' => Yii::$app->getUser()->getIdentity()->checkRole(['admin']),
                    'url' => ['/users/default/index'],
                ],
                [
                    'label' => 'Настройки',
                    'visible' => Yii::$app->getUser()->getIdentity()->checkRole(['admin']),
                    'items' => [
                        [
                            'label' => 'Настройки системы',
                            'url' => ['/config/default/index']
                        ],
                        [
                            'label' => 'Логи',
                            'url' => ['/logs/default/index']
                        ],
                    ]
                ],
                [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => yii\helpers\Url::to(['/users/default/logout']),
                    'linkOptions' => ['data-method' => 'post']
                ],
            ],
        ]);
    }
    NavBar::end();
    ?>
    <div class="container">
        <?= $content ?>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; test.work <?= date('Y') ?></p>
        <p class="pull-right"></p>
    </div>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
