<?php

namespace app\modules\logs;

use app\components\la\LAModule;

class Module extends LAModule
{

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }

}
