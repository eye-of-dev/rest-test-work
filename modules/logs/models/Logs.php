<?php

namespace app\modules\logs\models;

use app\modules\users\models\Users;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "logs".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $module
 * @property integer $item_id
 * @property string $message
 * @property string $create_date
 *
 * @property Users $user
 */
class Logs extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'item_id'], 'integer'],
            [['module', 'message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'module' => 'Модуль',
            'item_id' => 'ID объекта',
            'message' => 'Сообщение',
            'create_date' => 'Дата',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    /**
     * Добавление сообщения в лог
     *
     * @param $module
     * @param $item_id
     * @param $message
     */
    public static function log($module, $item_id, $message)
    {
        $model = new Logs;
        $model->module = $module;
        $model->item_id = $item_id;
        $model->message = $message;
        $model->user_id = Yii::$app->user->id;
        $model->save();
    }

}
