<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\logs\models\LogsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Логи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-logs-index">
    <div class="page-heading">
        <h1><i class="icon-tape"></i><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="widget">
        <div class="widget-content">
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'layout' => "{items}\n{pager}",
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        [
                            'attribute' => 'user_id',
                            'value' => function ($model){
                                return $model->user->username;
                            }
                        ],
                        'module',
                        'item_id',
                        'message:html',
                        'create_date',
                        [
                            'class' => \app\components\admin\LAActionColumn::className(),
                            'template' => '{delete}'
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
