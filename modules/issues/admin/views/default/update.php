<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\issues\models\Issues */

$this->title = Yii::t('issues', 'Изменение задания: ') . ' ' . $model->title;
?>
<div class="issues-update">
    <div class="page-heading">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
