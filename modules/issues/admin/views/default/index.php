<?php

use app\modules\issues\models\Issues;

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\issues\models\IssuesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('issues', 'Задания');
?>
<div class="issues-index">
    <div class="page-heading">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <p><?= Html::a(Yii::t('users', '<i class="icon-user-add"></i> Добавить задание', ['modelClass' => 'Users',]), ['create'], ['class' => 'btn btn-success']) ?></p>
    <div class="widget">
        <div class="widget-content">
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'layout' => "{items}\n{pager}",
                    'rowOptions' => function ($model, $key, $index, $grid) {
                        if ($model->is_delete == 1) {
                            return ['style' => 'background:#edd;'];
                        }
                    },
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'title',
                        [
                            'attribute' => 'user_id',
                            'value' => function ($model){
                                return $model->user->username;
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'value' => function ($model){
                                return Issues::$statuses[$model->status];
                            }
                        ],
                        'date_end',
                        [
                            'class' => \app\components\admin\LAActionColumn::className()
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

