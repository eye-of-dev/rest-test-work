<?php

use app\modules\issues\models\Issues;
use app\modules\users\models\Users;

use dosamigos\ckeditor\CKEditor;

use yii\jui\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\projects\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="projects-form">
    <div class="widget">
        <div class="widget-content padding">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                    'options' => ['rows' => 10],
                    'preset' => 'basic',
            ]) ?>

            <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(Users::findAll(['is_active' => 1]), 'id', 'username'), ['prompt' => 'Выберите пользователя']) ?>

            <?= $form->field($model, 'status')->dropDownList(Issues::$statuses, ['prompt' => 'Выберите статус']) ?>
            
            <?= $form->field($model, 'date_end')->widget(DatePicker::className(), [
                'options' => ['class' => 'form-control'],
                'dateFormat' => 'php:Y-m-d'
            ]); ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('users', 'Создать') : Yii::t('users', 'Сохранить'), ['class' => 'btn btn-success']) ?>
                <?= Html::a('Назад', Yii::$app->request->referrer, ['class' => 'btn btn-white btn-submit btn-submit-cancel']); ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
