<?php

namespace app\modules\issues\models;

use app\modules\logs\models\Logs;
use app\modules\users\models\Users;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "issues".
 */
class Issues extends ActiveRecord
{

    private $oldStatus;
    
    const STATUS_ONE = '1';
    const STATUS_TWO = '2';

    public static $statuses = array(
        self::STATUS_ONE => 'Статус 1',
        self::STATUS_TWO => 'Статус 2'
    );
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'issues';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'user_id', 'date_end'], 'required'],
            [['user_id', 'status', 'is_delete'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('issues', 'ID'),
            'title' => Yii::t('issues', 'Наименование'),
            'description' => Yii::t('issues', 'Описание'),
            'user_id' => Yii::t('issues', 'Исполнитель'),
            'status' => Yii::t('issues', 'Статус проекта'),
            'is_delete' => Yii::t('issues', 'Удален'),
            'date_end' => Yii::t('issues', 'Директивные сроки'),
            'create_date' => Yii::t('issues', 'Дата создания'),
            'update_date' => Yii::t('issues', 'Дата изменения')
        ];
    }

    public function behaviors()
    {
        return [
            'Timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date']
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

    public function afterFind()
    {
        $this->oldStatus = $this->status;
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        if (!$insert) {
            // логируем изменение статуса и время
            if ($this->status != $this->oldStatus) {
                $message = 'Статус изменен с: "' . self::$statuses[$this->oldStatus] . '" на: "' . self::$statuses[$this->status] . '"';
                Logs::log('issues', $this->id, $message);
            }
        }

        return parent::afterSave($insert, $changedAttributes);
    }
    
}
