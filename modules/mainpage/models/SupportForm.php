<?php

namespace app\modules\mainpage\models;

use yii\base\Model;

class SupportForm extends Model
{

    public $name;
    public $phone;
    public $message;

    public function rules()
    {
        return [
            [['name', 'phone', 'message'], 'required'],
        ];
    }

}
