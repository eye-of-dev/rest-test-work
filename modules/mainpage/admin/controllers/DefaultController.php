<?php

namespace app\modules\mainpage\admin\controllers;

use app\components\LAAController;
use app\modules\users\models\Users;

use Yii;
use yii\web\ErrorAction;

class DefaultController extends LAAController
{

    public function actionIndex()
    {
        if (Yii::$app->user->identity->role == Users::ROLE_USER) {
            return $this->redirect(['/promo/winners/index']);
        }
        return $this->redirect(['/users/default/index']);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::className(),
            ],
        ];
    }

}
