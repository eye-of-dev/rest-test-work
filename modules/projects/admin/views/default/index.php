<?php

use app\modules\projects\models\Projects;

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\projects\models\ProjectsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('projects', 'Проекты');
?>
<div class="projects-index">
    <div class="page-heading">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <p><?= Html::a(Yii::t('users', '<i class="icon-user-add"></i> Добавить проект', ['modelClass' => 'Users',]), ['create'], ['class' => 'btn btn-success']) ?></p>
    <div class="widget">
        <div class="widget-content">
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'layout' => "{items}\n{pager}",
                    'rowOptions' => function ($model, $key, $index, $grid) {
                        if ($model->is_delete == 1) {
                            return ['style' => 'background:#edd;'];
                        }
                    },
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'title',
                        'date_end',
                        'project_owner',
                        [
                            'attribute' => 'status',
                            'value' => function ($model){
                                return Projects::$statuses[$model->status];
                            }
                        ],
                        [
                            'class' => \app\components\admin\LAActionColumn::className()
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

