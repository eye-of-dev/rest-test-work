<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\projects\models\Projects */

$this->title = Yii::t('users', 'Изменение проекта: ') . ' ' . $model->title;
?>
<div class="users-update">
    <div class="page-heading">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
