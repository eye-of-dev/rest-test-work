<?php

namespace app\modules\projects\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "projects".
 */
class Projects extends ActiveRecord
{

    const STATUS_ONE = '1';
    const STATUS_TWO = '2';

    public static $statuses = array(
        self::STATUS_ONE => 'Статус 1',
        self::STATUS_TWO => 'Статус 2'
    );

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'date_start', 'date_end', 'description', 'user_id', 'project_owner', 'status'], 'required'],
            [['status', 'is_delete'], 'integer'],
            [['title', 'project_owner'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('projects', 'ID'),
            'title' => Yii::t('projects', 'Наименование'),
            'date_start' => Yii::t('projects', 'Дата создания'),
            'date_end' => Yii::t('projects', 'Дата окончания'),
            'description' => Yii::t('projects', 'Описание'),
            'user_id' => Yii::t('issues', 'Исполнитель'),
            'project_owner' => Yii::t('projects', 'Владелец проекта'),
            'status' => Yii::t('projects', 'Статус проекта'),
            'is_delete' => Yii::t('projects', 'Удален'),
            'create_date' => Yii::t('projects', 'Дата создания'),
            'update_date' => Yii::t('projects', 'Дата изменения')
        ];
    }

    public function behaviors()
    {
        return [
            'Timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date']
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

}
