<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\users\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('users', 'Пользователи');
?>
<div class="users-index">
    <div class="page-heading">
        <h1><i class="icon-users"></i> <?= Html::encode($this->title) ?></h1>
    </div>
    <p><?= Html::a(Yii::t('users', '<i class="icon-user-add"></i> Добавить пользователя', ['modelClass' => 'Users',]), ['create'], ['class' => 'btn btn-success']) ?></p>
    <div class="widget">
        <div class="widget-content">
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'layout' => "{items}\n{pager}",
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'username',
                        'create_date',
                        [
                            'class' => \app\components\admin\LAToggleColumn::className(),
                            'attribute' => 'is_active',
                        ],
                        [
                            'class' => \app\components\admin\LAActionColumn::className()
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

