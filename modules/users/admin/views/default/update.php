<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\users\models\Users */

$this->title = Yii::t('users', 'Изменение пользователя: ', ['modelClass' => 'Users',]) . ' ' . $model->username;
?>
<div class="users-update">
    <div class="page-heading">
        <h1><i class="icon-user"></i> <?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
