<?php

use app\modules\users\models\Users;

use yii\jui\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\users\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form">
    <div class="widget">
        <div class="widget-content padding">
            <?php $form = ActiveForm::begin([
                    'enableClientValidation' => ($model->isNewRecord) ? true : false,
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>

            <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255]) ?>

            <?= $form->field($model, 'first_name')->textInput(['maxlength' => 255]) ?>
            
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => 255]) ?>
            
            <?= $form->field($model, 'birthday')->widget(DatePicker::className(), [
                'options' => ['class' => 'form-control'],
                'dateFormat' => 'php:Y-m-d'
            ]); ?>
            
            <?= $form->field($model, 'role')->dropDownList(Users::$roles, ['prompt' => 'Выберите роль пользователя']) ?>

            <?= $form->field($model, 'avatar')->fileInput(); ?>

            <?php if($model->avatar): ?>
            <div class="form-group">
                <p><img src="<?= $model->getFilePath('avatar'); ?>" width="560"></p>
                <div class="help-block">
                    <p class="text-info">
                        <em>Загрузка нового отменяет старый</em>
                    </p>
                </div>
            </div>
            <?php endif; ?>
            
            <?= $form->field($model, 'is_active')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('users', 'Создать') : Yii::t('users', 'Сохранить'), ['class' => 'btn btn-success']) ?>
                <?= Html::a('Назад', Yii::$app->request->referrer, ['class' => 'btn btn-white btn-submit btn-submit-cancel']); ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
