<?php

namespace app\modules\users\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{

    public $username;
    public $password;
    private $_user = false;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['username', 'password'], 'required'],
            // email is exist by emailExist()
            ['username', 'usernameExist'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => Yii::t('users', 'E-mail пользвоателя'),
            'password' => Yii::t('users', 'Пароль')
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function usernameExist()
    {
        if (!$this->hasErrors()) {
            if (!$user = Users::findOne(['username' => $this->username])) {
                $this->addError('username', Yii::t('users', 'Пользователя с таким именем не существует'));
            }

            if ($user && !$user->is_active) {
                $this->addError('username', Yii::t('users', 'Пользователь с таким именем не прошел модерацию'));
            }
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError('password', 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser());
        }
        else {
            return false;
        }
    }

    /**
     * Finds user by [[email]]
     *
     * @return UserIdentity|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = UserIdentity::findByUsername($this->username);
        }

        return $this->_user;
    }

}
