<?php

namespace app\modules\users\models;

use app\components\traits\UploadableAsync;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "users".
 */
class Users extends ActiveRecord
{

    use UploadableAsync;
    
    private $oldPassword;

    const ROLE_GUEST = 'guest';
    const ROLE_USER = 'user';
    const ROLE_ADMIN = 'admin';

    public static $roles = array(
        self::ROLE_USER => 'Пользователь',
        self::ROLE_ADMIN => 'Администратор'
    );

    /**
     *
     * @var string 
     */
    private $_file1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password', 'role', 'birthday', 'avatar'], 'required'],
            [['username'], 'unique'],
            [['first_name', 'last_name'], 'safe'],
            [['is_active'], 'integer'],
            [['username', 'password', 'avatar'], 'string', 'max' => 255],
            [['avatar'], 'file', 'extensions' => 'jpg, png, gif'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('users', 'ID'),
            'username' => Yii::t('users', 'Имя пользователя'),
            'password' => Yii::t('users', 'Пароль'),
            'first_name' => Yii::t('users', 'Имя'),
            'last_name' => Yii::t('users', 'Фамилия'),
            'birthday' => Yii::t('users', 'День рождения'),
            'avatar' => Yii::t('users', 'Аватар'),
            'create_date' => Yii::t('users', 'Дата создания'),
            'update_date' => Yii::t('users', 'Дата изменения'),
            'is_active' => Yii::t('users', 'Активность'),
            'role' => Yii::t('users', 'Роль')
        ];
    }

    public function behaviors()
    {
        return [
            'Timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['create_date', 'update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['update_date']
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function afterFind()
    {
        $this->oldPassword = $this->password;

        $this->_file1 = $this->avatar;
    }

    public function beforeValidate()
    {
        if (!empty($this->password)) {
            $this->password = \Yii::$app->getSecurity()->generatePasswordHash($this->password);
        }
        else {
            $this->password = $this->oldPassword;
        }

        if (($file1 = UploadedFile::getInstance($this, 'avatar')) && $file1->error === 0) {
            $this->avatar = FileHelper:: saveUploaded($file1, self::tableName());
        }
        else {
            $this->avatar = $this->_file1;
        }

        return parent::beforeValidate();
    }

    public function isAdmin()
    {
        return in_array($this->role, ['admin']);
    }

    public function getUserFio()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

}
