<div class="starter-template">
    <h3>( API ) Web Service Tester</h3>
    <div class="row well">
        <div class="col-lg-12">
            <div class="col-lg-2">
                <div class="form-group">
                    <label class="control-label" for="method">Method</label>
                    <select name="method" class="form-control" id="reqmethod">
                        <option value="GET">GET</option>
                        <option value="POST">POST</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="form-group">
                    <label for="url" class="control-label">URL</label>
                    <input type="text" placeholder="URL" name="requrl" id="requrl" class="form-control">
                </div>
            </div>
            <div class="col-lg-1">
                <div style="margin-top: 25px;" class="form-group">
                    <input type="button" value="Send" style="font-weight:bold;font-size:16px;" class="btn btn-success" id="send-request">
                </div>
            </div>
        </div>
    </div>
    <div class="row well">
        <div id="params">
            <div class="row">
                <div class="col-lg-12" id="params_container">
                    <div class="row">
                        <div class="col-lg-12">
                            <a style="float: left;margin-bottom: 20px;" class="btn btn-info" id="btnaddparam" href="javascript:void(0)">+ Add Params</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row well" id="results">
        <pre>
            
        </pre>
    </div>
</div>
<div class="hidden">
    <div class="httpparameter">
        <div class="col-lg-12">
            <div class="col-lg-5">
                <div class="form-group">
                    <label class="control-label" for="method">Parameter Name</label>
                    <input type="text" class="form-control" name="parameter_name" value="" placeholder="Parameter Name">
                </div>
            </div>
            <div class="col-lg-5">
                <div class="form-group">
                    <label class="control-label" for="method">Parameter Value</label>
                    <input type="text" class="form-control" name="parameter_value" value="" placeholder="Parameter Value">
                </div>
            </div>
            <div class="col-lg-2">
                <div style="margin-top: 25px;" class="form-group">
                    <a class="close">&times;</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $script = <<< JS
 $("#btnaddparam").click(function(e) {
    e.preventDefault();
    $('.hidden > .httpparameter').clone(true).appendTo("#params_container");
});
        
$(".close").click(function(e) {
    e.preventDefault();
    $(this).parents('.httpparameter').remove();
});
        
$('#send-request').on('click', function(){

    var method = $('#reqmethod').val();
    var url = $.trim($('#requrl').val());
        
    var data = '';
        
    if(method === 'POST'){
        $('#params_container .httpparameter').each(function(){
            var parameter_name = $(this).find('input[name=parameter_name]').val();
            var parameter_value = $(this).find('input[name=parameter_value]').val();

            data += parameter_name + '=' + parameter_value + '&'
        });
    }
        
    $.ajax({
        type: method,
        url: url,
        data: data,
        dataType: 'json',
        success: function(data) {
            $('#results pre').text(JSON.stringify(data));
        }
    });
        
});
        
JS;
$this->registerJs($script);