<?php

namespace app\modules\rest\front\controllers;

use app\modules\users\models\LoginForm;
use app\modules\users\models\Users;
use app\modules\users\models\UsersAuthKeys;
use app\modules\users\models\UserIdentity;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class UsersController extends Controller
{

    private $enableCsrfValidationActions = [
        'login',
        'update'
    ];

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, $this->enableCsrfValidationActions)) {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionLogin()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new LoginForm;
        if (($model->attributes = Yii::$app->request->post()) && $model->login()) {
            return $this->getAuthKey();
        }
        else {
            return ['action' => 'login', 'errors' => CommonController::parseErrors($model->errors)];
        }
    }

    public function actionUpdate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Проверка на сущ. ключа
        if ($auth_key = Yii::$app->request->get('auth_key')) {
            $user_id = Yii::$app->request->get('user_id');

            // проверка на срок и актуальность ключа
            $result = self::checkAuthKey($auth_key, $user_id);
            if ($result === TRUE) {

                $model = Users::findOne($user_id);
                $model->password = '';
                
                $postFields = Yii::$app->request->post();
                if (($model->attributes = Yii::$app->request->post()) && $model->save()) {

                    $user[] = [
                        'id' => $model->id,
                        'username' => $model->username,
                        'first_name' => $model->first_name,
                        'last_name' => $model->last_name,
                        'birthday' => $model->birthday,
                        'avatar' => $model->getFilePath('avatar')
                    ];

                    return ['action' => 'user-update', 'result' => $user];
                }
                else {
                    return ['action' => 'user-update', 'errors' => CommonController::parseErrors($model->errors)];
                }
            }
            else {
                return $result;
            }
        }

        return ['action' => 'user-update', 'errors' => 'Oтсутствует токен авторизации'];
    }

    /**
     * 
     * @return array
     */
    private function getAuthKey()
    {
        $model = new UsersAuthKeys;
        $model->user_id = Yii::$app->user->identity['id'];
        $model->auth_key = Yii::$app->security->generateRandomString();
        $model->expires = time() + 3600;

        if ($model->save()) {
            return [
                'action' => 'login',
                'user_id' => Yii::$app->user->identity['id'],
                'auth_key' => $model->auth_key
            ];
        }
        else {
            return ['action' => 'auth_key', 'errors' => CommonController::parseErrors($model->errors)];
        }
    }

    /**
     * Check user permission
     * @param string $user_id
     * @return boolean
     */
    public static function checkPermission($user_id)
    {

        if ($user_id) {
            $model = Users::findOne($user_id);

            return $model->isAdmin();
        }

        return FALSE;
    }

    /**
     * Check auth key
     * @param string $auth_key
     * @param string $user_id
     * @return boolean|array
     */
    public static function checkAuthKey($auth_key = NULL, $user_id = NULL)
    {
        $result = UsersAuthKeys::findOne(['auth_key' => $auth_key, 'user_id' => $user_id]);

        if ($result) {
            if ($result->expires > time()) {
                $user = UserIdentity::findIdentity($user_id);
                Yii::$app->user->login($user);
                return TRUE;
            }
            else {
                return ['action' => 'old_auth_key', 'errors' => ['auth_key' => 'Срок действия ключа истек. Авторизируйтесь заново.']];
            }
        }
        else {
            return ['action' => 'invalid_auth_key', 'errors' => ['auth_key' => 'Введен несуществующий ключ']];
        }
    }

}
