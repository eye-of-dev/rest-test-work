<?php

namespace app\modules\rest\front\controllers;

use app\modules\projects\models\Projects;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class ProjectsController extends Controller
{

    private $enableCsrfValidationActions = [
        'project-create',
        'project-update',
    ];

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, $this->enableCsrfValidationActions)) {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionProjectsList()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Проверка на сущ. ключа
        if ($auth_key = Yii::$app->request->get('auth_key')) {
            $user_id = Yii::$app->request->get('user_id');

            // проверка на срок и актуальность ключа
            $result = UsersController::checkAuthKey($auth_key, $user_id);
            if ($result === TRUE) {

                $projects = [];

                $query = Projects::find();

                // получаем права
                // если админ покахывается все проекты
                // если пользователь только проекты назначеные ему
                if (!UsersController::checkPermission($user_id)) {
                    $query->where(['user_id' => $user_id, 'is_delete' => 0]);
                }
                $results = $query->all();

                foreach ($results as $v) {
                    $projects[] = ['id' => $v->id, 'title' => $v->title];
                }

                return ['action' => 'projects-list', 'result' => $projects];
            }
            else {
                return $result;
            }
        }

        return ['action' => 'projects-list', 'errors' => 'Oтсутствует токен авторизации'];
    }

    public function actionProjectInfo()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Проверка на сущ. ключа
        if ($auth_key = Yii::$app->request->get('auth_key')) {
            $user_id = Yii::$app->request->get('user_id');

            if (!($project_id = Yii::$app->request->get('project_id'))) {
                return ['action' => 'project-info', 'errors' => 'Oтсутствует идентификатор проекта'];
            }

            // проверка на срок и актуальность ключа
            $result = UsersController::checkAuthKey($auth_key, $user_id);
            if ($result === TRUE) {

                $query = Projects::find();
                $query->where(['id' => $project_id]);
                if ($result = $query->one()) {

                    $project[] = [
                        'id' => $result->id,
                        'title' => $result->title,
                        'date_start' => $result->date_start,
                        'date_end' => $result->date_end,
                        'description' => $result->description,
                        'project_owner' => $result->project_owner,
                        'status' => ['id' => $result->status, 'title' => Projects::$statuses[$result->status]],
                    ];

                    return ['action' => 'projects-info', 'result' => $project];
                }
                else {
                    return ['action' => 'projects-info', 'errors' => 'Проект не найден'];
                }
            }
            else {
                return $result;
            }
        }

        return ['action' => 'projects-info', 'errors' => 'Oтсутствует токен авторизации'];
    }

    public function actionProjectCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Проверка на сущ. ключа
        if ($auth_key = Yii::$app->request->get('auth_key')) {
            $user_id = Yii::$app->request->get('user_id');

            // проверка на срок и актуальность ключа
            $result = UsersController::checkAuthKey($auth_key, $user_id);
            if ($result === TRUE) {

                $model = new Projects;
                $model->user_id = $user_id;

                $postFields = Yii::$app->request->post();
                if (($model->attributes = Yii::$app->request->post()) && $model->save()) {
                    return ['action' => 'project-create', 'result' => [
                            'id' => $model->id,
                            'title' => $model->title
                    ]];
                }
                else {
                    return ['action' => 'project-create', 'errors' => CommonController::parseErrors($model->errors)];
                }
            }
            else {
                return $result;
            }
        }

        return ['action' => 'project-create', 'errors' => 'Oтсутствует токен авторизации'];
    }

    public function actionProjectUpdate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Проверка на сущ. ключа
        if ($auth_key = Yii::$app->request->get('auth_key')) {
            $user_id = Yii::$app->request->get('user_id');

            if (!($project_id = Yii::$app->request->get('project_id'))) {
                return ['action' => 'project-update', 'errors' => 'Oтсутствует идентификатор проекта'];
            }

            // проверка на срок и актуальность ключа
            $result = UsersController::checkAuthKey($auth_key, $user_id);
            if ($result === TRUE) {

                $model = Projects::findOne($project_id);
                $model->user_id = $user_id;

                $postFields = Yii::$app->request->post();
                if (($model->attributes = Yii::$app->request->post()) && $model->save()) {

                    $project[] = [
                        'id' => $model->id,
                        'title' => $model->title,
                        'date_start' => $model->date_start,
                        'date_end' => $model->date_end,
                        'description' => $model->description,
                        'project_owner' => $model->project_owner,
                        'status' => ['id' => $model->status, 'title' => Projects::$statuses[$model->status]],
                    ];

                    return ['action' => 'project-update', 'result' => $project];
                }
                else {
                    return ['action' => 'project-update', 'errors' => CommonController::parseErrors($model->errors)];
                }
            }
            else {
                return $result;
            }
        }

        return ['action' => 'project-update', 'errors' => 'Oтсутствует токен авторизации'];
    }

    public function actionProjectDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Проверка на сущ. ключа
        if ($auth_key = Yii::$app->request->get('auth_key')) {
            $user_id = Yii::$app->request->get('user_id');

            if (!($project_id = Yii::$app->request->get('project_id'))) {
                return ['action' => 'project-delete', 'errors' => 'Oтсутствует идентификатор проекта'];
            }

            // проверка на срок и актуальность ключа
            $result = UsersController::checkAuthKey($auth_key, $user_id);
            if ($result === TRUE) {

                Yii::$app->db->createCommand("UPDATE `projects` SET `is_delete`=1 WHERE `id`=:id", [':id' => $project_id])->execute();
                return ['action' => 'project-delete', 'result' => 'Проект удален'];
            }
            else {
                return $result;
            }
        }

        return ['action' => 'project-delete', 'errors' => 'Oтсутствует токен авторизации'];
    }

}
