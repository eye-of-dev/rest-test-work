<?php

namespace app\modules\rest\front\controllers;

use app\components\LAController;

class ToolsController extends LAController
{

    public function actionConsole()
    {
        return $this->render('console');
    }

}
