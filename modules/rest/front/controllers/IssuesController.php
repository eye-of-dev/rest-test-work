<?php

namespace app\modules\rest\front\controllers;

use app\modules\issues\models\Issues;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class IssuesController extends Controller
{

    private $enableCsrfValidationActions = [
        'issue-create',
        'issue-update',
    ];

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, $this->enableCsrfValidationActions)) {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionIssuesList()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Проверка на сущ. ключа
        if ($auth_key = Yii::$app->request->get('auth_key')) {
            $user_id = Yii::$app->request->get('user_id');

            // проверка на срок и актуальность ключа
            $result = UsersController::checkAuthKey($auth_key, $user_id);
            if ($result === TRUE) {

                $query = Issues::find();

                // получаем права
                // если админ покахывается все проекты
                // если пользователь только проекты назначеные ему
                if (!UsersController::checkPermission($user_id)) {
                    $query->where(['user_id' => $user_id, 'is_delete' => 0]);
                }
                $results = $query->all();

                $issues = [];
                foreach ($results as $v) {
                    $issues[] = ['id' => $v->id, 'title' => $v->title];
                }

                return ['action' => 'issues-list', 'result' => $issues];
            }
            else {
                return $result;
            }
        }

        return ['action' => 'issues-list', 'errors' => 'Oтсутствует токен авторизации'];
    }

    public function actionIssueInfo()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Проверка на сущ. ключа
        if ($auth_key = Yii::$app->request->get('auth_key')) {
            $user_id = Yii::$app->request->get('user_id');
            
            if (!($issue_id = Yii::$app->request->get('issue_id'))) {
                return ['action' => 'issues-info', 'errors' => 'Oтсутствует идентификатор задания'];
            }

            // проверка на срок и актуальность ключа
            $result = UsersController::checkAuthKey($auth_key, $user_id);
            if ($result === TRUE) {

                $query = Issues::find();
                $query->where(['id' => $issue_id]);
                if ($result = $query->one()) {

                    $project[] = [
                        'id' => $result->id,
                        'title' => $result->title,
                        'description' => $result->description,
                        'user_id' => $result->user_id,
                        'status' => ['id' => $result->status, 'title' => Issues::$statuses[$result->status]],
                        'date_end' => $result->date_end,
                    ];

                    return ['action' => 'issues-info', 'result' => $project];
                }
                else {
                    return ['action' => 'issues-info', 'errors' => 'Задание не найдено'];
                }
            }
            else {
                return $result;
            }
        }

        return ['action' => 'issues-info', 'errors' => 'Oтсутствует токен авторизации'];
    }

    public function actionIssueCreate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Проверка на сущ. ключа
        if ($auth_key = Yii::$app->request->get('auth_key')) {
            $user_id = Yii::$app->request->get('user_id');

            // проверка на срок и актуальность ключа
            $result = UsersController::checkAuthKey($auth_key, $user_id);
            if ($result === TRUE) {

                $model = new Issues;
                $model->user_id = $user_id;

                $postFields = Yii::$app->request->post();
                if (($model->attributes = Yii::$app->request->post()) && $model->save()) {
                    return ['action' => 'issue-create', 'result' => [
                            'id' => $model->id,
                            'title' => $model->title
                    ]];
                }
                else {
                    return ['action' => 'issue-create', 'errors' => CommonController::parseErrors($model->errors)];
                }
            }
            else {
                return $result;
            }
        }

        return ['action' => 'issue-create', 'errors' => 'Oтсутствует токен авторизации'];
    }

    public function actionIssueUpdate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Проверка на сущ. ключа
        if ($auth_key = Yii::$app->request->get('auth_key')) {
            $user_id = Yii::$app->request->get('user_id');

            if (!($issue_id = Yii::$app->request->get('issue_id'))) {
                return ['action' => 'issue-update', 'errors' => 'Oтсутствует идентификатор задания'];
            }

            // проверка на срок и актуальность ключа
            $result = UsersController::checkAuthKey($auth_key, $user_id);
            if ($result === TRUE) {

                $model = Issues::findOne($issue_id);
                $model->user_id = $user_id;

                $postFields = Yii::$app->request->post();
                if (($model->attributes = Yii::$app->request->post()) && $model->save()) {

                    $project[] = [
                        'id' => $model->id,
                        'title' => $model->title,
                        'description' => $model->description,
                        'user_id' => $model->user_id,
                        'status' => ['id' => $model->status, 'title' => Issues::$statuses[$model->status]],
                        'date_end' => $model->date_end,
                    ];

                    return ['action' => 'issue-update', 'result' => $project];
                }
                else {
                    return ['action' => 'issue-update', 'errors' => CommonController::parseErrors($model->errors)];
                }
            }
            else {
                return $result;
            }
        }

        return ['action' => 'project-update', 'errors' => 'Oтсутствует токен авторизации'];
    }

    public function actionIssueDelete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        //Проверка на сущ. ключа
        if ($auth_key = Yii::$app->request->get('auth_key')) {
            $user_id = Yii::$app->request->get('user_id');

            if (!($issue_id = Yii::$app->request->get('issue_id'))) {
                return ['action' => 'issue-delete', 'errors' => 'Oтсутствует идентификатор задания'];
            }

            // проверка на срок и актуальность ключа
            $result = UsersController::checkAuthKey($auth_key, $user_id);
            if ($result === TRUE) {

                Yii::$app->db->createCommand("UPDATE `issues` SET `is_delete`=1 WHERE `id`=:id", [':id' => $issue_id])->execute();
                return ['action' => 'issue-delete', 'result' => 'Задание удалено'];
            }
            else {
                return $result;
            }
        }

        return ['action' => 'issue-delete', 'errors' => 'Oтсутствует токен авторизации'];
    }

}
