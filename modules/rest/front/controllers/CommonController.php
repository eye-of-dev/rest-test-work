<?php

namespace app\modules\rest\front\controllers;

use yii\web\Controller;

class CommonController extends Controller
{

    /**
     * Parse errors
     * @return array
     */
    public static function parseErrors($errors = [])
    {
        foreach ($errors as $error) {
            return $error['0'];
        }
    }

}
